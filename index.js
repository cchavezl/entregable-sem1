function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */

  var clave = txtClave.value;
  var valor = txtValor.value;

  sessionStorage.setItem(clave, valor);

  var objeto = {
    nombre:"Cinthia",
    apellidos:"Chávez León",
    ciudad:"Lima",
    pais:"Perú"
  };

  sessionStorage.setItem("json", JSON.stringify(objeto));
}
function leerDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);

  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor; /*innerText sustituye el spanValor del html por el valor de la clave */

  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}

function contarDatosEnSessionStorage(){
  contar = sessionStorage.length;

  var spanCuenta = document.getElementById("spanCuenta");
  spanCuenta.innerText = contar; /*innerText sustituye el spanValor del html por el valor de la clave */

}

function eliminarDeSessionStorage(){
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);

  sessionStorage.removeItem(clave,valor);
}

function destruirSessionStorage(){
  sessionStorage.clear();
}
